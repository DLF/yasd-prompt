import unittest
from yasd.merger import DictMerger

class TestDictPrimitiveAttribute(unittest.TestCase):
    
    def test_attribute_in_a_but_not_in_b_is_taken_from_a(self):
        a = {'foo': 'bar'}
        b = {}
        self.assertEqual(
            {'foo': 'bar'},
            DictMerger().merge(a, b)
        )

