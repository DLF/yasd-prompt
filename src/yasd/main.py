#!/usr/bin/env python

import argparse
import sys
from typing import Optional
import yaml

from dataclasses import dataclass

from yasd.merger import PresetMerger


@dataclass
class Preset:
    positive: str
    negative: str
    steps: int
    sampler: str
    cfg_scale: int
    size_x: int
    size_y: int
    model: str
    denoising_strength: float


def cli(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('presets', nargs='*')
    args = parser.parse_args(argv)
    if len(args.presets) == 0:
        print("No presets given. By.")
    preset_dict: Optional[dict] = None
    merger = PresetMerger()
    for preset_file_arg in args.presets:
        with open(preset_file_arg, "r") as preset_file:
            this_dict = yaml.safe_load(preset_file)
            if preset_dict is None:
                preset_dict = this_dict
            else:
                



def main():
    cli(sys.argv[1:])


if __name__ == "__main__":
    main()



