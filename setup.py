#!/usr/bin/env python
from setuptools import setup

setup(
    name="yasd-prompt",
    version="0.0.1",
    description="",
    author="",
    author_email="",
    license="GPL2",
    url="",
    package_dir={'': 'src'},
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=[
        "yasd",
    ],
    install_requires=[
        "argparse",
        "pyyaml",
        "pytest",
    ],
    entry_points="""
    [console_scripts]
    yasd=yasd.main:main
    """,
)
