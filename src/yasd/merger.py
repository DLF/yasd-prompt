class DifferentTypesCantBeMerged(Exception):
    def __init__(self, key, type1, type2) -> None:
        self.key = key
        self.type1 = type1
        self.type2 = type2
        self.msg = "Key {key} has type {t1} in first, and {t2} in second dictionary. Different type cannot be merged.".format(
            key = key,
            t1 = type1,
            t2 = type2,
        )
        super().__init__(self.msg)

        

class DictMerger:
    def __init__(self, concat: dict[str,str]):
        self._concat_map = concat

    def merge(self, d1: dict, d2: dict) -> dict:
        keys1 = list(d1.keys())
        keys2 = list(d2.keys())
        result = {}
        for k in keys1:
            if k not in keys2:
                result[k] = d1[k]
        return result


class PresetMerger(DictMerger):
    def __init__(self):
        super().__init__(
            concat={
                'positive': ', ',
                'negativ': ', ',
            }
        )


